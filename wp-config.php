<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'greenland_florist_db' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vs,c:$vvME2Mq:xhCuh8n/l-*c/^:]=h1_w@/FH(TrYmFJi;u3,K8k<Q/4t-]vaV' );
define( 'SECURE_AUTH_KEY',  '2f+~Jwk(C^m(.fzF-]WvuVY1Z+<QOB_(g_Kg4_`*!1rZyI4{L_!erxe:/QQ*_|ct' );
define( 'LOGGED_IN_KEY',    '2{tAhcp;B /@bBVur?,YIQ83cSRS/;lZ]M7~/^7P49aaE1`rc$?]@6xu98|5Bh>@' );
define( 'NONCE_KEY',        'n!o`gx47*2*s7E@efkN/m*aG?s9a4)|y]RHwyVf)s==rb|#*|eT-3=V_Z+Kcy)AW' );
define( 'AUTH_SALT',        '9GM1|N*?gdc[B]}O0XRbhs)U2)Cn]jTto=dGcRe^#o2BcJgEN4%-k2gDLKkHF?Ls' );
define( 'SECURE_AUTH_SALT', 'ph;Y_avd3T5C`aK(])OLZQd_3@@_1.waR>+FY=4YCOay#6[*?1TJoBB/5{4!Q7~d' );
define( 'LOGGED_IN_SALT',   '%eh%%U|KFwKI]FI7I}iMg{,f2 @Rn,vz1kV@pk.?4tK`dz@7PzpIP^VV&p/U.`a?' );
define( 'NONCE_SALT',       'S*$,WUCe%masG lsitaS8a[218o>]ppD:$vq7KeM[^^nid%Rl!*3_l(5%=MZw]f(' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
